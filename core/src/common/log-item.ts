// ????
export class LogItem {
  public type: string;
  public date: any;

  constructor(
    type: (string | null) = null,
    date: (any | null) = null,
  ) {
    if(type) this.type = type;
    if(date) this.date = date;
  }

  public toString(): string {
    let str = '';
    str += `type=${this.type};`;
    str += `date=${this.date};`;
    return str;
  }

  // public toJSON(): String {
  //   return JSON.stringify(this);
  // }

}
