import { Injectable, Inject } from '@angular/core';

import { IAuthRouteInfo, AuthRouteInfo } from './auth-route-info';


@Injectable()
export class DefaultAuthRoutes {

  public authRouteInfo: IAuthRouteInfo = new AuthRouteInfo();
  constructor() {
  }

  // constructor(public authRouteInfo: IAuthRouteInfo = new AuthRouteInfo()) {}
  // constructor( @Inject('AuthRouteInfo') public authRouteInfo: IAuthRouteInfo) {}

  // constructor(private _authRouteInfo: (IAuthRouteInfo | null)) {
  //   if(!_authRouteInfo) {
  //     this._authRouteInfo = new AuthRouteInfo();
  //   }
  // }

  // public getAuthRouteInfo(): IAuthRouteInfo {
  //   return this._authRouteInfo;
  // }


}
