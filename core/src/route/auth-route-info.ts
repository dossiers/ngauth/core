import { Injectable } from '@angular/core';

export interface IAuthRouteInfo {
  readonly home: (string | null);      // Default page. E.g., to be redirected when the user logs out.
  readonly login: (string | null);
  readonly logout: (string | null);
  readonly register: (string | null);  // New registration
  readonly confirm: (string | null);   // Confirm registration
  readonly resend: (string | null);    // Resend registration code
  readonly change: (string | null);    // Password change (user initiated)
  readonly reset: (string | null);     // Password reset (forgot password)
  // ...
}

// @Injectable()
export class AuthRouteInfo implements IAuthRouteInfo {

  public constructor(
    public home: (string | null) = null,
    public login: (string | null) = null,
    public logout: (string | null) = null,
    public register: (string | null) = null,
    public confirm: (string | null) = null,
    public resend: (string | null) = null,
    public change: (string | null) = null,
    public reset: (string | null) = null,
  ) {
  }

  // For debugging....
  public toString(): string {
    let str = '';
    str += `home = ${this.home}; `;
    str += `login = ${this.login}; `;
    str += `logout = ${this.logout}; `;
    str += `register = ${this.register}; `;
    str += `confirm = ${this.confirm}; `;
    str += `resend = ${this.resend}; `;
    str += `change = ${this.change}; `;
    str += `reset = ${this.reset}; `;
    // ...
    return str;
  }
}

