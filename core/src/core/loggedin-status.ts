export class LoggedInStatus {

  constructor(
    public message: string,
    public loggedIn: boolean
  ) {
  }

  public toString(): string {
    let str = '';
    str += `message=${this.message};`;
    str += `loggedIn=${this.loggedIn};`;
    return str;
  }
}
