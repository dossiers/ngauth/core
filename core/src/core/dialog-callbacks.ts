
export interface DialogParentCallback {
  // action is just a signal/label/information to the callback client.
  dialogOperationDone(action: (string | null)): void;
}

