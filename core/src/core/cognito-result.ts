import { CommonResult } from './common-result';


// TBD:
export class CognitoResult extends CommonResult {

  constructor(
    public message: (string | null) = null,
    public result: (any | null) = null,
    public extra: (any | null) = null
  ) {
    super(message, result, extra);
  }
  

}
