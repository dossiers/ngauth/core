# @ngauth/core

## Overview

`NgAuth` is a suite of Angular modules
comprising a set of auth-related reusable components and services.
The library is mostly frontend/UI, and it uses AWS Cognito for serverless backend.
The core module includes shared classes and common utilities.
It is not meant to be used by itself.


_(Note: this library is currently in beta,_
_and the APIs may change over time.)_



## Installation

To install this library, run:

```bash
$ npm install --save @ngauth/core
```



## Using the library

From your Angular `AppModule`:

```typescript
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

// Import the library
import { NgAuthCoreModule } from '@ngauth/core';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,

    // Specify the library as an import
    NgAuthCoreModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

Once the library is imported, you can use its components, directives and pipes in your Angular application.



## License

MIT © [Harry Y](mailto:sidewaybot@gmail.com)
